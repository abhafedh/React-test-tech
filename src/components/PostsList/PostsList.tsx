import * as _ from 'lodash';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchPosts } from '../../actions';

import './PostsList.css';

class PostsListComponent extends React.Component<any, any> {
	public componentDidMount() {
		this.props.fetchPosts();
	}
	public renderPosts = () => {
		return _.map(this.props.posts, (post, index) => {
			return (
				<Link to={`/posts/${post.id}`} key={index} className="list-group-item">
					<h4>{post.title}</h4>
				</Link>
			);
		});
	};
	public render() {
		return (
			<div className="postsList">
				<div>
					<Link className="btn btn-primary" to="/posts/new">
						Add a post
					</Link>
				</div>
				<h3>Posts :</h3>
				<ul className="list-group">{this.renderPosts()}</ul>
			</div>
		);
	}
}

function mapStateToProps({ posts }: any) {
	return { posts };
}

export const PostsList = connect(
	mapStateToProps,
	{ fetchPosts }
)(PostsListComponent);
