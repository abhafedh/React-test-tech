import axios from 'axios';

const ROOT_URL = 'http://reduxblog.herokuapp.com/api';
const API_KEY = '?key=8757RGD';

export const FETCH_POSTS: 'FETCH_POSTS' = 'FETCH_POSTS';
export const FETCH_POST: 'FETCH_POST' = 'FETCH_POST';
export const CREATE_POST: 'CREATE_POST' = 'CREATE_POST';

// ----- FETCH POSTS ----- //
export type FetchPostsAction = () => any;

export const fetchPosts: FetchPostsAction = () => {
	return (dispatch: any) => {
		return axios.get(`${ROOT_URL}/posts${API_KEY}`).then(response => {
			return dispatch({
				payload: response,
				type: FETCH_POSTS,
			});
		});
	};
};

// ----- CREATE POST ----- //
export interface ICreatePostValues {
	categories: string;
	content: string;
	title: string;
}

export type CreatePostAction = (values: ICreatePostValues, callback: () => void) => any;

export const createPost: CreatePostAction = (values, callback) => {
	return (dispatch: any) => {
		return axios.post(`${ROOT_URL}/posts${API_KEY}`, values).then(response => {
			callback();
			dispatch({
				payload: response,
				type: CREATE_POST,
			});
		});
	};
};

// ----- FETCH POST ----- //
export type FetchPostAction = (id: number | null) => any;

export const fetchPost: FetchPostAction = (id: any) => {
	return (dispatch: any) => {
		return axios.get(`${ROOT_URL}/posts/${id}${API_KEY}`).then(response => {
			dispatch({
				payload: response,
				type: FETCH_POST,
			});
		});
	};
};

// ----- DELETE POST ----- //
// You have to create the action and use axios.delete(...
// You can use _.omit(data, id)
