import * as React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import './App.css';
import './index.css';

import { PostNew, PostShow, PostsList } from './components';
import reducers from './reducers';
const createStoreWithMiddleware = applyMiddleware(thunk, logger)(createStore);

class App extends React.Component {
	public render() {
		return (
			<Provider store={createStoreWithMiddleware(reducers)}>
				<BrowserRouter>
					<div className="App">
						<Switch>
							<Route path="/posts/new" component={PostNew} />
							<Route path="/posts/:id" component={PostShow} />
							<Route path="/" component={PostsList} />
						</Switch>
					</div>
				</BrowserRouter>
			</Provider>
		);
	}
}

export default App;
