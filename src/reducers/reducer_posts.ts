import * as _ from 'lodash';
import { FETCH_POST, FETCH_POSTS, fetchPost, fetchPosts } from '../actions';

export type PostAction = ReturnType<typeof fetchPost> | ReturnType<typeof fetchPosts>;

export const postsReducer = (state = {}, action: PostAction) => {
	switch (action.type) {
		case FETCH_POST:
			return {
				...state,
				[action.payload.data.id]: action.payload.data,
			};
		case FETCH_POSTS:
			return _.mapKeys(action.payload.data, 'id');
		default:
			return state;
	}
};
