## Blog Test

### React + Redux + TypeScript + Thunk + Axios

This is a very tiny blog created with **React, Redux, TypeScript, Redux Thunk and Axios** based on **create-react-app** with typeScript

To launch the project it's simple :

```
yarn

yarn start
```

Here's the features you'll fin in the blog :

-   List of the blog posts
-   Add a new blog post
-   Open a specific blog post
-   **Delete a blog post**

**For the exercice we removed the possiblity to delete a blog post and the purpose is to recreate it.**

_Bonus: If you want to make it properly, you can redirect the user to the posts list page using `this.props.history.push('/')` at the right place :)_

This is the only files you need to edit :

`src/components/PostShow/PostShow.ts`

`src/reducers/reducer_posts.ts`

`src/actions/index.ts`

-   You have to use `axios.delete(...` to remove the post via the API
-   You can use `lodash.omit` to remove the post form the store

## Tips

The posts collection in the store is created like this :

```
{
  987987: {...},
  987988: {...},
  987989: {...},
  ...
}
```
